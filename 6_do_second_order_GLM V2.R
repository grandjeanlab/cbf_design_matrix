
#name of your table, assuming it is in the working directory
folder_list<-('CBF_DM_20200131_ANTs_JvA.csv')

#output folder where the model will be saved
export_folder<-'mod_20200131'

dir.create(export_folder,recursive = T,showWarnings = F)

#reads the table. as of 20200206, it comes with 11 empty columns
study_list<-read.csv(folder_list,header=T,sep=';')
level()

#initiate a new column TIMEPOINT_norm with NA values
study_list$TIMEPOINT_norm<-rep(NA, dim(study_list)[1])
#Euglycemic time points 
study_list$TIMEPOINT_norm[study_list$TIMEPOINT %in% c("EuIV1","EuIV2", "EuIV3",  "EuIV4")]<-0
study_list$TIMEPOINT_norm[study_list$TIMEPOINT %in% c("Hypo1", "Hypo2", "Hypo3","Hypo4", "Hypo5","Hypo6", "Hypo7")]<-1
study_list<-study_list[!is.na(study_list$TIMEPOINT_norm),]

study_list$group<-as.factor(paste(study_list$INFUSION,study_list$TIMEPOINT_norm,sep='_'))


ID_factor<-levels(factor(study_list$PARTICIPANT))
Group_factor<-levels(factor(study_list$group))


ID_design<-c()
Group_design<-c()

ID_group<-c()


file_list<-c()

for(w in 1:dim(study_list)[1])
{

    file_list[w]<-as.character(study_list$CBFMAP_STDSPACE[w])
    ID_vec<-rep(0,length(ID_factor))
    ID_vec[which(ID_factor==study_list$PARTICIPANT[w])]<-1
    ID_design<-rbind(ID_design,ID_vec)
    ID_group<-c(ID_group,which(ID_factor==study_list$PARTICIPANT[w]))
    
    Group_vec<-rep(0,length(Group_factor))
    Group_vec[which(Group_factor==study_list$condition[w])]<-1
  Group_design<-rbind(Group_design,Group_vec)

}

final_design<-cbind(Group_design,ID_design)
write.table(file_list,paste(export_folder,'/','scan_list.txt',sep=''),row.names = F,col.names = F,quote = F)
final_design_length_adjust<-c(rep(0,length(ID_factor)))

##set design now just for  Group_design
#Group_factor
#[1] "Lactate_0" "Lactate_1" "Placebo_0" "Placebo_1"



K<-rbind(c(-1,1,1,-1, final_design_length_adjust),
         c(1,-1,-1,1, final_design_length_adjust),
         c(1,-1,0,0, final_design_length_adjust),
         c(-1,1,0,0, final_design_length_adjust),
         c(0,0,1,-1, final_design_length_adjust),
         c(0,0,-1,1, final_design_length_adjust))

con_name<-c("Interaction1","Interaction2","Lac_EU>Lac_HY","Lac_EU<Lac_HY","Pla_EU>Pla_HY","Pla_EU<Pla_HY")





desgin_mat<-c()
ii<-1
desgin_mat[ii]<-paste('/NumWaves',  dim(final_design)[2],sep="\t")
ii<-ii+1
desgin_mat[ii]<-paste('/NumPoints',  dim(final_design)[1],sep="\t")
ii<-ii+1
desgin_mat[ii]<-paste('/PPheights\t', paste(rep('1',dim(final_design)[1]),collapse = '\t'),sep="\t")
ii<-ii+1
desgin_mat[ii]<-''
ii<-ii+1
desgin_mat[ii]<-paste('/Matrix',sep="\t")
for(i in 1:dim(final_design)[1]){
  ii<-ii+1
  desgin_mat[ii]<-paste(as.character(final_design[i,]),collapse = '\t')
  
}


write.table(desgin_mat,'design.mat',quote = F,row.names = F,col.names = F)



desgin_grp<-c()
ii<-1
desgin_grp[ii]<-paste('/NumWaves',  '1',sep="\t")
ii<-ii+1
desgin_grp[ii]<-paste('/NumPoints',  dim(final_design)[1],sep="\t")
ii<-ii+1
desgin_grp[ii]<-''
ii<-ii+1
desgin_grp[ii]<-paste('/Matrix',sep="\t")
for(i in 1:length(ID_group)){
  ii<-ii+1
  desgin_grp[ii]<-paste(as.character(ID_group[i]),collapse = '\t')
  
}


write.table(desgin_grp,'design.grp',quote = F,row.names = F,col.names = F)






desgin_con<-c()
ii<-1
for(i in 1:dim(K)[1]){
  desgin_con[ii]<-paste(paste('/ContrastName',i,sep=''),  con_name[i],sep="\t")
  ii<-ii+1
}
desgin_con[ii]<-paste('/NumWaves',  dim(K)[2],sep="\t")
ii<-ii+1
desgin_con[ii]<-paste('/NumContrasts',  dim(K)[1],sep="\t")
ii<-ii+1
desgin_con[ii]<-paste('/PPheights\t', paste(rep('1',dim(K)[1]),collapse = '\t'),sep="\t")
ii<-ii+1
desgin_con[ii]<-paste('/RequiredEffect\t', paste(rep('1',dim(K)[1]),collapse = '\t'),sep="\t")
ii<-ii+1
desgin_con[ii]<-''
ii<-ii+1
desgin_con[ii]<-paste('/Matrix',sep="\t")
for(i in 1:dim(K)[1]){
  ii<-ii+1
  desgin_con[ii]<-paste(as.character(K[i,]),collapse = '\t')
  
}



write.table(desgin_con,'design.con',quote = F,row.names = F,col.names = F)


