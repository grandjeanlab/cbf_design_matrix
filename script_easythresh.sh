#!/usr/bin/env bash
#
# 20200218: JvA+JG; way to cluster the z-values in groups by thresholding (because randomise was too strict)
# 1.95 ~ 2*SD, 0.05 p-value threshold
#
# execute this script in the mod_yyyymmdd subdirectory, where the design matrices and list of scans are defined
#
ZSTAT_DIR=zstat
CWD=$(pwd)

[ ! -d ${ZSTAT_DIR} ] && mkdir -p ${ZSTAT_DIR}

if [ -s z_stat.nii.gz -a -s mask_tmp.nii.gz ]; then #-a is logical AND
	fslsplit z_stat.nii.gz ${ZSTAT_DIR}/z
else
	echo -e "\tFirst generate z_stat.nii.gz and/or mask_tmp.nii.gz files and rerun this script \"${0}\"."
	exit 0
fi

cd ${ZSTAT_DIR}
for contrast in $(ls *.nii.gz); do 
	pwd
	echo "Processing: ${contrast} ... to ${contrast/.nii.gz/}"
	echo "easythresh ${contrast} ../mask_tmp.nii.gz 1.95 0.05 /usr/share/fsl/5.0/data/standard/MNI152_T1_2mm.nii.gz ${contrast/.nii.gz/}"
	easythresh ${contrast} ../mask_tmp.nii.gz 1.95 0.05 /usr/share/fsl/5.0/data/standard/MNI152_T1_2mm.nii.gz ${contrast/.nii.gz/}
done
cd ${CWD}

