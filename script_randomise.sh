#!/usr/bin/env bash
# execute this script in the mod_yyyymmdd subdirectory, where the design matrices and list of scans are defined

mask='/usr/share/fsl/5.0/data/standard/MNI152_T1_2mm_brain_mask_dil.nii.gz'

fslmerge -t merged $(cat scan_list.txt)

# For consistency we estimated a mask where voxels were covered in 100% of the subjects. 
fslmaths merged -Tmin -bin mask_tmp

randomise -i merged.nii.gz -o rand -d design.mat -t design.con -m mask_tmp -e design.grp -T -n 1000 -D

#according to JG a more lenient test:
fsl_glm -i merged.nii.gz -m mask_tmp.nii.gz -d design.mat -c design.con --demean --out_p=p_stat --out_z=z_stat
